// const csv2xml = require('./src/index.js')
const $ = require( 'jquery' )
const fs = require( 'fs' )
const {
    dialog
} = require( 'electron' ).remote
const shell = require( 'electron' ).shell
const mime = require( 'mime' )
const Papa = require( 'papaparse' )
const papa_config = {
    delimiter: ",", // auto-detect
    newline: "", // auto-detect
    header: true,
    dynamicTyping: false,
    preview: 0,
    encoding: "utf-8",
    worker: false,
    comments: false,
    step: undefined,
    complete: undefined,
    error: undefined,
    download: false,
    skipEmptyLines: true,
    chunk: undefined,
    fastMode: false,
    beforeFirstChunk: undefined,
    withCredentials: undefined
}

const pjson = require( './package.json' )

var csv2xml_desktop = {
    csvString: '',
    xml: '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>\n<Root xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">\n',
    reset: function(){
        this.csvString = '',
        this.xml = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>\n<Root xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">\n'
    }
}
exports = module.exports = csv2xml_desktop


csv2xml_desktop.init = ( function () {

    const fileInput = $( '#in_file' )
    const submit_btn = $( '#submit' )
    const help_btn = $( '#help' )
    const info_btn = $( '#info' )
    const log_links = $( '#log_console' )

    document.title = pjson.name
    $( 'footer' ).html( '<small>v' + pjson.version + '</small>' )

    csv2xml_desktop.reset()


    function parseCSV( data ) {
        let parsed = Papa.parse( data, papa_config )
        for ( let i = 0; i < parsed.data.length; i++ ) {
            processRow( parsed.data[ i ], parsed.meta.fields )
        }
        callback()
    }

    function processRow( data_row, meta_fields ) {
        let row = ''
        row += '\t<Row>' + '\n'

        for ( let j = 0; j < meta_fields.length; j++ ) {
            meta_fields[ j ] = meta_fields[ j ].replace( /\s/g, '_' )
            row += '\t\t' + '<' + meta_fields[ j ] + '>\n'
            // console.log( data_row[ meta_fields[ j ] ] )
            let text_content = data_row[ meta_fields[ j ] ]
            text_content = text_content.replace(/&/g,'and')
            row += '\t\t\t' + text_content + '\n'
            // row += '\t\t\t' + data_row[ meta_fields[ j ] ] + '\n'
            row += '\t\t' + '</' + meta_fields[ j ] + '>' + '\n'
        }

        row += '\t</Row>' + '\n'
        csv2xml_desktop.xml += row
    }

    function callback() {
        csv2xml_desktop.xml += '</Root>'

        var newName = fileInput[ 0 ].files[ 0 ].name
        newName = newName.replace( ".csv", "" )

        var out = dialog.showSaveDialog( {
            title: 'Save XML as...',
            defaultPath: newName + '.xml',
            buttonLabel: '' //,
                // filters: ['xml']
        } )

        fs.writeFile( out, csv2xml_desktop.xml, 'utf-8', ( err ) => {
            if ( err ) {
                log2console( 'Error ' + err, 'red' )
                throw err
            }
            log2console( '... done', null )
            var str = 'Saved to ' + out
            log2console( str, 'lightblue' )
        } )
    }

    function log2console( str, col ) {

        if(!col){
            col = 'white'
        }

        let pre = '<p style="color:' + col + '">'
        let post = '</p>'
        str = pre + str + post
        $( str ).appendTo( $( '#log_console' ) )

        $( '#log_console' ).animate( {
            scrollTop: $( '#log_console' ).prop( 'scrollHeight' )
        }, 400 )

    }


    fileInput.on( 'change', function ( e ) {

        csv2xml_desktop.reset()
        $('#log_console')[0].textContent = ''

        var csv = fileInput[ 0 ].files[ 0 ].path

        fs.readFile( csv, 'utf8', function ( err, data ) {
            if ( err ) throw err
            csv2xml_desktop.csvString = data
        } )

        var file_size = ( function () {
            let fsize
            if ( fileInput[ 0 ].files[ 0 ].size < 1000 ) {
                fsize = fileInput[ 0 ].files[ 0 ].size + " byte"
            } else if ( fileInput[ 0 ].files[ 0 ].size >= 1000 && fileInput[ 0 ].files[ 0 ].size <
                1000 * 1024 ) {
                fsize = ( fileInput[ 0 ].files[ 0 ].size / 1000 ).toFixed( 2 ) + " KB"
            } else if ( fileInput[ 0 ].files[ 0 ].size > ( 1000 * 1024 ) ) {
                fsize = ( fileInput[ 0 ].files[ 0 ].size / 1000 / 1024 ).toFixed( 2 ) + " MB"
            }
            return fsize
        } )()

        if ( mime.lookup( fileInput[ 0 ].files[ 0 ].path ) !== "text/csv" ) {
            log2console( 'The input file must have a MIME type of "text/csv"', 'red' )
            $( 'form' )[ 0 ].reset()
        } else {
            log2console( 'File: ' + fileInput[ 0 ].files[ 0 ].name )
            log2console( 'Date: ' + fileInput[ 0 ].files[ 0 ].lastModifiedDate )
            log2console( 'Size: ' + file_size )
            $( '#submit' ).attr( 'disabled', false )
            log2console( 'Press "process" to generate XML file', 'limegreen' )
        }

    } )

    submit_btn.on( 'click', function ( e ) {
        e.preventDefault();
        log2console( 'Working ...', null )
        parseCSV( csv2xml_desktop.csvString )
            // log2console( '... done', null )
    } )

    help_btn.on( 'click', function ( e ) {
        let str = 'For ' + e.target.id +' go to'
        str +=
            '<br><a href="https://bitbucket.org/datafunk/csv2xml-desktop/wiki/Home" target="_blank">bitbucket.org/datafunk/csv2xml-desktop/wiki/Home</a>'

        log2console( str, 'limegreen' )
    } )

    info_btn.on( 'click', function ( e ) {
        log2console( '<big>csv2xml</big> converter utility (v' + pjson.version + ') powered by:',
            'lightgrey' )
        log2console( '- node (' + process.versions.node + ')', 'lightgrey' )
        log2console( '- chrome (' + process.versions.chrome + ')', 'lightgrey' )
        log2console( '- electron (' + process.versions.electron + ')', 'lightgrey' )
    } )

    log_links.on( 'click', 'a[href^="http"]', function ( e ) {
        e.preventDefault();
        shell.openExternal( this.href );
    } )


    var checkVersion = ( function () {

        const https = require( 'https' )
        const current = pjson.version
            // var latest

        function compare( latest, current ) {
            // console.log( [ latest, current ] )


            var latest_arr = latest.split( '.' )
            var current_arr = current.split( '.' )

            var latest_num = (parseInt(latest_arr[0]) * 1000000) + (parseInt(latest_arr[1]) * 1000) + (parseInt(latest_arr[2]) * 1)
            var current_num = (parseInt(current_arr[0]) * 1000000) + (parseInt(current_arr[1]) * 1000) + (parseInt(current_arr[2]) * 1)
            // console.log( [ latest_num, current_num, latest_num > current_num ] )

            if ( latest_num > current_num ) {
                log2console( process.platform, 'lightred' )
                log2console( 'Update ' + latest + ' available.', 'lightred' )
                log2console(
                    'Download it now: <a href="https://bitbucket.org/datafunk/csv2xml-desktop/raw/master/app/win/csv2xml.zip" target="_blank">https://bitbucket.org/datafunk/csv2xml-desktop/raw/master/app/win/</a>'
                )
            }
            // else {
            //     log2console( 'you are on the latest version' )
            // }
        }



        let req = function ( data ) {
            data.setEncoding( 'utf-8' )

            data.on( 'error', function ( error ) {
                console.error( error )
                log2console( 'Could not check against latest version', 'lightred' )
            } )

            data.on( 'data', function ( res ) {
                compare( res, current )
                // isSemVer( res, current )
            } )

        }

        https.get( pjson.config.control_version_url, req )
    } )()



} )()
