# CSV to XML transformer Desktop

This is a fork of git@bitbucket.org:datafunk/csv2xml.git node util to create a desktop app.

## End User Documentation

For non-technical / end-user instructions and overview, please see the [Wiki](https://bitbucket.org/datafunk/csv2xml-desktop/wiki/Home)

## Development instructions

```
git clone git@bitbucket.org:datafunk/csv2xml-desktop.git
# keeps link to nodejs module for easy updates
git remote add upstream git@bitbucket.org:datafunk/csv2xml.git
git pull upstream master
git push origin master
```

## Build

This workflow assumes globally installed _electron-packager CLI_

```
# osx
npm run build-osx

# win 32
npm run build-win
```

__Building native executables must be done on the target platform!__
The app was originally developed on OSX (El Captain) using Vagrant and ModernIE for Windows builds.
See the included ```vagrantfile``` which should be good to go (as of Nov 2016). Do note that the initial download if the windows image may take quite a while. Go and do something else, while it downloads.

_My typical setup was that I shared the project directory in OSX with the VM, so I can build directly back to my repo's WC_

You will also need [Git-bash](https://git-for-windows.github.io/), if not for git itself, but to get a decent terminal experience. That is where I run ```npm run build-win```



### In-app version awareness

To enable a cheap way to notify users if there's an update available ```node_modules/.hooks/electron-packager/task.js``` writes a ```version.md``` file into the project root. This is accessed at https://bitbucket.org/datafunk/csv2xml-desktop/raw/master/version.md and parsed to compare against current installed version.
